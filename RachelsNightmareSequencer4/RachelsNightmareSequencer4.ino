// Arduino synth library absolute frequency example

//Hardware connections:

//                    +10µF
//PIN 11 ---[ 1k ]--+---||--->> Audio out
//                  |
//                 === 10nF
//                  |
//                 GND


#include <synth.h>
#include "GPIO.h"

synth edgar;    //-Make a synth

// http://forum.arduino.cc/index.php?topic=6549.msg51570#msg51570
// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

void setupFastADC()
{
  // http://forum.arduino.cc/index.php?topic=6549.msg51570#msg51570
  // set prescale to 16
  sbi(ADCSRA, ADPS2) ;
  cbi(ADCSRA, ADPS1) ;
  cbi(ADCSRA, ADPS0) ;
}

void setup() {

  setupFastADC();
  Serial.begin(250000);

  edgar.begin();                                   //-Start it up
  edgar.setupVoice(0, SINE, 60, ENVELOPE2, 120, 64); //-Set up voice 0
  edgar.setupVoice(1, SINE, 60, ENVELOPE2, 120, 64); //-Set up voice 0
  edgar.setupVoice(2, SINE, 60, ENVELOPE2, 120, 64); //-Set up voice 0
  edgar.setupVoice(3, SINE, 60, ENVELOPE2, 120, 64); //-Set up voice 0
}

// A6 A4 A2 A1
// A7 A5 A3 A0

// D2 D4 D6 D8
// D3 D5 D7

//0-1024

uint16_t convertrange(uint16_t raw, uint16_t maxv)
{
  uint16_t v;

  v = (raw / 1024.0) * maxv;

  Serial.print("[");
  Serial.print(raw);
  Serial.print(":");
  Serial.print(maxv);
  Serial.print(":");
  Serial.print(v);
  Serial.print("]");

  return v;
}

byte voice = 0;
byte editvoice = 0;

GPIO<BOARD::D2> Button1;
GPIO<BOARD::D4> Button2;
GPIO<BOARD::D6> Button3;
GPIO<BOARD::D8> Button4;
GPIO<BOARD::D3> Button5;
GPIO<BOARD::D5> Button6;
GPIO<BOARD::D7> Button7;

void loop()
{
  bool btn1, btn2, btn3, btn4, btn5, btn6, btn7;
  uint16_t pot0, pot1, pot2, pot3, pot4, pot5, pot6, pot7;
  uint16_t v0, v1, v2, v3, v4, v5, v6, v7;

  Button1.input();
  Button2.input();
  Button3.input();
  Button4.input();
  Button5.input();
  Button6.input();
  Button7.input();

  Button1.high();
  Button2.high();
  Button3.high();
  Button4.high();
  Button5.high();
  Button6.high();
  Button7.high();

  btn1 = Button1.read();
  btn2 = Button2.read();
  btn3 = Button3.read();
  btn4 = Button4.read();
  btn5 = Button5.read();
  btn6 = Button6.read();
  btn7 = Button7.read();

  Button1.low();
  Button2.low();
  Button3.low();
  Button4.low();
  Button5.low();
  Button6.low();
  Button7.low();

  switch (voice)
  {
    case 0:
      Button1.output();
      Button1.low();
      break;
    case 1:
      Button2.output();
      Button2.low();
      break;
    case 2:
      Button3.output();
      Button3.low();
      break;
    case 3:
      Button4.output();
      Button4.low();
      break;
  }

  if (btn1 == 0)
  {
    editvoice = 0;
  }

  if (btn2 == 0)
  {
    editvoice = 1;
  }

  if (btn3 == 0)
  {
    editvoice = 2;
  }

  if (btn4 == 0)
  {
    editvoice = 3;
  }

  pot0 = analogRead(6);
  pot1 = analogRead(4);
  pot2 = analogRead(2);
  pot3 = analogRead(1);
  pot4 = analogRead(7);
  pot5 = analogRead(5);
  pot6 = analogRead(3);
  pot7 = analogRead(0);

  v0 = convertrange(pot0, 3 + 1);
  v1 = convertrange(pot1, 6 + 1);
  v2 = convertrange(pot2, 127 + 1);
  v3 = convertrange(pot3, 4 + 1);
  v4 = convertrange(pot4, 127 + 1);
  v5 = convertrange(pot5, 127 + 1);
  v6 = convertrange(pot6, 2000 + 1);
  v7 = convertrange(pot7, 1000 + 1);

  edgar.setupVoice(editvoice, v1, v2, v3, v4, v5);
  edgar.setFrequency(editvoice, v6);

  edgar.trigger(voice);

  Serial.print(editvoice);
  Serial.print(" ");
  Serial.print(voice);
  Serial.print(" ");
  Serial.print(btn1);
  Serial.print(" ");
  Serial.print(btn2);
  Serial.print(" ");
  Serial.print(btn3);
  Serial.print(" ");
  Serial.print(btn4);
  Serial.print(" ");
  Serial.print(v0);
  Serial.print(" ");
  Serial.print(v1);
  Serial.print(" ");
  Serial.print(v2);
  Serial.print(" ");
  Serial.print(v3);
  Serial.print(" ");
  Serial.print(v4);
  Serial.print(" ");
  Serial.print(v5);
  Serial.print(" ");
  Serial.print(v6);
  Serial.print(" ");
  Serial.print(v7);
  Serial.println(" ");

  delay(v7);

  if (++voice == 4)
  {
    voice = 0;
  }

}