// Arduino synth library absolute frequency example

//Hardware connections:

//                    +10µF
//PIN 11 ---[ 1k ]--+---||--->> Audio out
//                  |
//                 === 10nF
//                  |
//                 GND

// synth parameters for all 4 buttons get loaded from eeprom on startup
// press a blue button to play the synth note saved for that button
// press left yellow button to move the selection blue light to the next blue button
// the sequence is off,1,2,3,4,off
// press middle yellow button to play the synth note selected by the pots
// press the right yellow button or the selected blue button to save the synth parameters for that blue button
// write those values to eeprom

#include <synth.h>
#include "GPIO.h"

#define DEBOUNCEV 10

//Get VOICEDATASIZE with Serial.println(sizeof(voiceData_t));
#define VOICEDATASIZE 24
#define EEPROMADD 10

struct voiceData_t
{
  byte Init1;
  unsigned char Ver1;

  byte VoiceData[20];

  unsigned char Ver2;
  byte Init2;
}
VoiceData;

synth edgar;    //-Make a synth

GPIO<BOARD::D2> Button1;
GPIO<BOARD::D4> Button2;
GPIO<BOARD::D6> Button3;
GPIO<BOARD::D8> Button4;
GPIO<BOARD::D3> Button5;
GPIO<BOARD::D5> Button6;
GPIO<BOARD::D7> Button7;

byte selvoice = 0;

bool v_lightbutton[] = {0, 0, 0, 0, 0, 0, 0};
byte v_debouncebutton[] = {0, 0, 0, 0, 0, 0, 0};
uint16_t v_pot[] = {0, 0, 0, 0, 0, 0, 0};

// http://forum.arduino.cc/index.php?topic=6549.msg51570#msg51570
// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

void LoadVoices()
{
  eeprom_read_block((void*)&VoiceData, (void*)EEPROMADD, VOICEDATASIZE);
}


void SaveVoices()
{
  eeprom_write_block((const void*)&VoiceData, (void*)EEPROMADD, VOICEDATASIZE);
}

void SetupFastADC()
{
  // http://forum.arduino.cc/index.php?topic=6549.msg51570#msg51570
  // set prescale to 16
  sbi(ADCSRA, ADPS2) ;
  cbi(ADCSRA, ADPS1) ;
  cbi(ADCSRA, ADPS0) ;
}

// A6 A4 A2 A1
// A7 A5 A3 A0

// D2 D4 D6 D8
// D3 D5 D7

// 0-1024

void Pad(uint16_t raw, uint16_t maxv)
{
  if (maxv > 99)
  {
    if (raw < 10)
    {
      Serial.print(F("00"));
    }
    else if (raw < 100)
    {
      Serial.print(F("0"));
    }
  }
}

uint16_t ConvertRange(uint16_t raw, uint16_t maxv)
{
  uint16_t v;
  float maxraw;

  // lower the point at which maxv is reached for small values of maxv, divides dial into equal sectors
  // for large values of maxv, dial needs to be maxed out to reach it.
  maxraw = (maxv < 10) ? 900 : 1023;
  v = maxv * (raw / maxraw);
  v = (v > maxv) ? maxv : v; // clips v to maxv

  Serial.print(F("["));
  //Serial.print(raw);
  //Serial.print(F(":"));
  //Serial.print(maxv);
  //Serial.print(F(":"));
  Pad(v, maxv);
  Serial.print(v);
  Serial.print(F("]"));

  return v;
}

void ReadPots()
{
  v_pot[0] = analogRead(6);
  v_pot[1] = analogRead(4);
  v_pot[2] = analogRead(2);
  v_pot[3] = analogRead(1);
  v_pot[4] = analogRead(7);
  v_pot[5] = analogRead(5);
  v_pot[6] = analogRead(3);
  v_pot[7] = analogRead(0);
}

void PlayFree(byte wave, byte pitch, byte env, byte len, byte mod)
{
  for (byte i = 0; i < 255; i++)
  {
    for (byte k = 0; k < 4; k++)
    {
      if (edgar.voiceFree(k) == 1)
      {
        edgar.setupVoice(k, wave, pitch, env, len, mod);
        edgar.trigger(k);
        return;
      }
    }
  }
}

void PlayAt(byte voice, byte wave, byte pitch, byte env, byte len, byte mod)
{
  edgar.setupVoice(voice, wave, pitch, env, len, mod);
  edgar.trigger(voice);
}

// be careful with this, if the button is set both to output and high, pressing a button shorts the arduino pin with 5v to ground
// this function is called after setting the pin to input to enable the builtin pullup resistors
void ButtonHigh(byte button)
{
  switch (button)
  {
    case 0:
      Button1.high();
      break;
    case 1:
      Button2.high();
      break;
    case 2:
      Button3.high();
      break;
    case 3:
      Button4.high();
      break;
    case 4:
      Button5.high();
      break;
    case 5:
      Button6.high();
      break;
    case 6:
      Button7.high();
      break;
  }
}

void ButtonLow(byte button)
{
  switch (button)
  {
    case 0:
      Button1.low();
      break;
    case 1:
      Button2.low();
      break;
    case 2:
      Button3.low();
      break;
    case 3:
      Button4.low();
      break;
    case 4:
      Button5.low();
      break;
    case 5:
      Button6.low();
      break;
    case 6:
      Button7.low();
      break;
  }
}

void ButtonInput(byte button)
{
  switch (button)
  {
    case 0:
      Button1.input();
      break;
    case 1:
      Button2.input();
      break;
    case 2:
      Button3.input();
      break;
    case 3:
      Button4.input();
      break;
    case 4:
      Button5.input();
      break;
    case 5:
      Button6.input();
      break;
    case 6:
      Button7.input();
      break;
  }
}

void ButtonOutput(byte button)
{
  switch (button)
  {
    case 0:
      Button1.output();
      break;
    case 1:
      Button2.output();
      break;
    case 2:
      Button3.output();
      break;
    case 3:
      Button4.output();
      break;
    case 4:
      Button5.output();
      break;
    case 5:
      Button6.output();
      break;
    case 6:
      Button7.output();
      break;
  }
}

//can be used for raw, debounced access to buttons, returns 0 if pressed
byte ButtonRead(byte button)
{
  byte v = 1;

  ButtonHigh(button);
  ButtonInput(button);

  switch (button)
  {
    case 0:
      v = Button1.read();
      break;
    case 1:
      v = Button2.read();
      break;
    case 2:
      v = Button3.read();
      break;
    case 3:
      v = Button4.read();
      break;
    case 4:
      v = Button5.read();
      break;
    case 5:
      v = Button6.read();
      break;
    case 6:
      v = Button7.read();
      break;
  }

  if (v_lightbutton[button])
  {
    ButtonLow(button);
    ButtonOutput(button);
  }
  else
  {
    ButtonHigh(button);
    ButtonInput(button);
  }

  return v;
}

void SetLight(byte light, byte value)
{
  if (light < 7)
  {
    v_lightbutton[light] = value;
  }
}

void AllLightsOff()
{
  for (byte i = 0; i < 7; i++)
  {
    SetLight(i, 0);
  }
}

void AllLightsOn()
{
  for (byte i = 0; i < 7; i++)
  {
    SetLight(i, 1);
  }
}

bool IsPressed(byte btn)
{
  bool v = v_debouncebutton[btn] == DEBOUNCEV;

  return v;
}

// call this first in the loop() function

void UpdateButton()
{
  for (byte i = 0; i < 7; i++)
  {
    if ((ButtonRead(i) == 0) && (v_debouncebutton[i] == 0))
    {
      v_debouncebutton[i] = DEBOUNCEV;
    }
  }
}

// call this last in the loop() function
void Debounce()
{
  for (byte i = 0; i < 7; i++)
  {
    v_debouncebutton[i] = (byte)((v_debouncebutton[i] > 0) ? v_debouncebutton[i] - 1 : 0);

    if ((v_debouncebutton[i] == 1) && (ButtonRead(i) == 0)) // dont allow debounce to clear if button still pressed
    {
      v_debouncebutton[i] = 2;
    }
  }
}

void setup() {

  SetupFastADC();
  Serial.begin(9600); // max 250000

  edgar.begin();                                   //-Start it up
  LoadVoices();

  /*
    //no need to setup synths, they will be setup on realtime when a button is pressed
    edgar.setupVoice(0, SINE, 60, ENVELOPE2, 120, 64); //-Set up voice 0
    edgar.setupVoice(1, SINE, 60, ENVELOPE2, 120, 64); //-Set up voice 0
    edgar.setupVoice(2, SINE, 60, ENVELOPE2, 120, 64); //-Set up voice 0
    edgar.setupVoice(3, SINE, 60, ENVELOPE2, 120, 64); //-Set up voice 0
  */
}

void loop()
{
  //holds values for interactive middle yellow button
  byte wave, pitch, env, len, mod;
  //holds values for the currently pressed blue button
  byte wave1, pitch1, env1, len1, mod1;

  UpdateButton();
  ReadPots();

  //*********************************************************************
  //  Setup all voice parameters in MIDI range
  //  voice[0-3],wave[0-6],pitch[0-127],envelope[0-4],length[0-127],mod[0-127:64=no mod]
  //*********************************************************************
  //void setupVoice(unsigned char voice, unsigned char wave, unsigned char pitch, unsigned char env, unsigned char length, unsigned int mod)

  // convert the potentiometer values into synth parameters to play with middle yellow button
  pitch = ConvertRange(v_pot[0], 127);
  wave = ConvertRange(v_pot[1], 6);
  env = ConvertRange(v_pot[2], 4);
  len = ConvertRange(v_pot[3], 117); // higher values result in infinete lenght sounds
  mod = ConvertRange(v_pot[4], 127);

  // check to see if a blue button is pressed
  for (byte i = 0; i < 4; i++)
  {
    // always load the data for the current blue button, even if it is not pressed
    // attempt to make the loop execute in the same time, even if it takes longer
    wave1 = VoiceData.VoiceData[(i) * 5 + 0];
    pitch1 = VoiceData.VoiceData[(i) * 5 + 1];
    env1 = VoiceData.VoiceData[(i) * 5 + 2];
    len1 = VoiceData.VoiceData[(i) * 5 + 3];
    mod1 = VoiceData.VoiceData[(i) * 5 + 4];

    if (IsPressed(i))
    {
      //move wave1..mod1 loading here if faster overall execution is desired
      //play synth note from saved data
      PlayFree(wave1, pitch1, env1, len1, mod1);
    }
  }

  //left yellow button
  // illuminate blue light in sequence, then turn off again
  if (IsPressed(4))
  {
    if (++selvoice == 5)
    {
      selvoice = 0;
    }

    AllLightsOff();
    if (selvoice > 0)
    {
      SetLight(selvoice - 1, 1);
    }
  }

  //middle yellow button
  //play synth note dictated by pots
  if (IsPressed(5))
  {
    PlayFree(wave, pitch, env, len, mod);
  }

  //right yellow button
  //save pots to currently selected blue button
  if (IsPressed(6))
  {
    if (selvoice > 0)
    {
      VoiceData.VoiceData[(selvoice - 1) * 5 + 0] = wave;
      VoiceData.VoiceData[(selvoice - 1) * 5 + 1] = pitch;
      VoiceData.VoiceData[(selvoice - 1) * 5 + 2] = env;
      VoiceData.VoiceData[(selvoice - 1) * 5 + 3] = len;
      VoiceData.VoiceData[(selvoice - 1) * 5 + 4] = mod;
      SaveVoices();
    }
  }

  Serial.println(F(""));
  Debounce();
}